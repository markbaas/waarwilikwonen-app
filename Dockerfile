FROM ubuntu:latest

RUN apt-get update && apt-get -y install npm git curl

RUN npm install -g n http-server
RUN n lts

COPY / /app
WORKDIR /app
RUN npm install
RUN ls -l
RUN npm run build

EXPOSE 80
CMD [ "http-server", "/app/build", "80" ]