import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import 'react-mdl/extra/material.css'
import 'react-mdl/extra/material.js'
import 'dialog-polyfill/dialog-polyfill.css'

import mapApp from './reducers'
import App from './components/App'
import watchers from './watchers'

const loggerMiddleware = createLogger()

const preloadedState = { layers: [] }

const store = createStore(mapApp, preloadedState,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    ))
const rootEl = document.getElementById('root')

const render = () => ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    rootEl
)

render()
store.subscribe(render)
watchers.subscribe(store)