export const apikey = (state = {}, action) => {
    switch (action.type) {
        case 'RECEIVE_APIKEY':
            return Object.assign({}, state, {
              apikey: action.apikey,
              expires: action.expires
            })
        default:
            return state
    }
}
