export const dialog = (state = true, action) => {
    switch (action.type) {
        case 'DIALOG_CHANGED':
            return action.open
        default:
            return state
    }
}
