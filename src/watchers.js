import watch from 'redux-watch'

import { fetchData, fetchRegionCentroids } from './actions/data'
import { fetchVectorLayer } from './actions/layers'
import { isLoading } from './actions/misc'


function subscribe(store) {
  function store_subscribe(key, func) {
    store.subscribe(watch(store.getState, key)(func))
  }

  store_subscribe('apikey', (newVal, oldVal, objPath) => {
    store.dispatch(fetchRegionCentroids())
    store.dispatch(fetchVectorLayer())
    store.dispatch(isLoading(true))
  })

  store_subscribe('scores.centroids', (newVal, oldVal, objPath) => {
    store.dispatch(fetchData())
  })

  store_subscribe('scores', (newVal, oldVal, objPath) => {
    if (Object.keys(newVal).length === 4) {
      store.dispatch(isLoading(false))
    }
  })
}

export default {
  subscribe
}