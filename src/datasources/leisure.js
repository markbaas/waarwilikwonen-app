import _ from '../translate'


const leisure = [
  { key: 'nightlife', label: _`Nightlife` },
  { key: 'bars', label: _`Bars and pubs` },
  { key: 'museums', label: _`Museums` },
  { key: 'theatre', label: _`Theatre` },
  { key: 'cinema', label: _`Movies theatres` },
  { key: 'theme_parks', label: _`Attraction parks` },
  { key: 'sports', label: _`Sports` }
]

export default leisure