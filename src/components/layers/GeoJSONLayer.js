import { Component, PropTypes } from 'react'

class GeoJSONLayer extends Component {
    render() {
        const map = this.context.map
        const { data } = this.props
        const id = data.properties.name
        const sourceOptions = {
            type: "geojson",
            data: data
        }
        const options = {
            id: data.properties.name,
            type: "fill",
            source: data.properties.name,
            layout: {},
            paint: {
                'fill-color': '#088',
                'fill-opacity': 0.1
            }
        }
        try {
            map.removeSource(id)
            map.removeLayer(id)
        } catch(err) { }

        map.addSource(data.properties.name, sourceOptions)
        map.addLayer(options)
        return null
    }
}

GeoJSONLayer.propTypes = {
    map: PropTypes.object,
    data: PropTypes.object
}

GeoJSONLayer.contextTypes = {
    map: PropTypes.object.isRequired,
}

export default GeoJSONLayer