import React, { PropTypes } from 'react'

import { Textfield } from 'react-mdl'
import Dropdown from './Dropdown'


class Select extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false
    }
  }

  closeDialog = () => {
    this.setState({ open: false })
  }

  showDialog() {
    this.setState({ open: true })
  }

  render() {
    const { children, label, value, onChange, required } = this.props
    const labelValueMap = {}
    children.forEach((child) => {
      labelValueMap[child.props.value] = child.props.children
    })
    return (
      <div className="mdlp-select">
        <Dropdown onActivate={(child) => {
              this.setState({ open: false, value: child.props.value })
              onChange(child.props.value)
        }}
          selected={value}
          open={this.state.open}
          onClose={this.closeDialog}
          anchor={this}
          >
          {children}
        </Dropdown>
        <input type="hidden" value={value !== undefined && value}/>
        <Textfield
          label={label}
          onClick={(e) => {
            this.showDialog()
          }}
          floatingLabel={value !== undefined}
          value={labelValueMap[value] || ''}
          required={required}
        />
      </div>
    )
  }
}

Select.propTypes = {
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.any,
    required: PropTypes.bool
}

export default Select