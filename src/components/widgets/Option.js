import React, { PropTypes } from 'react'
import { Button } from 'react-mdl'

class Option extends React.Component {
  render() {
    const { selected, value, children, onClick } = this.props
    const style = {
      width: '100%',
      textTransform: 'none'
    }
    if (selected) {
      style.color = 'rgb(255, 64, 129)'
    }
    return (
      <Button
        type="button"
        onClick={onClick}
        value={value}
        style={style}
      >{children}</Button>
    )
  }
}

Option.propTypes = {
    value: PropTypes.any,
    onClick: PropTypes.func,
    selected: PropTypes.bool
}

export default Option