import React, { PropTypes } from 'react'

import { Button } from 'react-mdl'
import uuidV4 from 'uuid'


const ID = '_grecaptcha.element.id';

const removeChild = elem => elem.parentNode && elem.parentNode.removeChild(elem);


class RecaptchaButton extends React.Component {
  constructor() {
    super()
    this.buttonId = uuidV4()
  }

  componentDidMount() {
    const { callback, sitekey } = this.props;

    // 1. Async lazy load
    const head = document.head || document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.id = ID;
    script.src = `https://www.google.com/recaptcha/api.js?onload=_grecaptcha_onload&render=explicit`;
    script.type = 'text/javascript';
    script.async = true;
    script.defer = true;
    script.onerror = (oError) => {
      throw new URIError(`The script ${oError.target.src} is not accessible.`);
    };

    // 2. Expose callback function to window object
    window['_grecaptcha_onload'] = () => {
      grecaptcha.render( // eslint-disable-line
        this.buttonId, { sitekey, callback })
    }

    head.appendChild(script);

  }

  componentWillUnmount() {
    removeChild(document.getElementById(ID));
  }

  render() {
    const { accent, colored, component, name, primary, raised, ripple, onClick } = this.props
    const { sitekey } = this.props;

    return (
      <Button
        id={this.buttonId}
        onClick={onClick}
        accent={accent} colored={colored} component={component} name={name}
        primary={primary} raised={raised} ripple={ripple}
        data-sitekey={sitekey}
        data-callback='_grecaptcha_callback'
      >{this.props.children}</Button>
    )
  }
}

RecaptchaButton.propTypes = {
  sitekey: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
}

Object.keys(Button.propTypes).forEach((key) => {
  RecaptchaButton.propTypes[key] = Button.propTypes[key]
})

export default RecaptchaButton