import React from 'react'
import { findDOMNode } from 'react-dom'
import dialogPolyfill from 'dialog-polyfill'

import { Dialog as _Dialog } from 'react-mdl'

class Dialog extends _Dialog {
  componentDidMount() {
    const dialog = findDOMNode(this)
    if (!dialog.showModal) {   // avoid chrome warnings and update only on unsupported browsers
      dialogPolyfill.registerDialog(dialog);
      this.setState({ isPolyfill: true })
    } else {
      this.setState({ isPolyfill: false })
    }

    super.componentDidMount()
  }

  componentDidUpdate(prevProps) {
    const dialog = findDOMNode(this)

    if (this.props.open !== prevProps.open) {
      if (this.props.open) {
        if (this.props.modal === false) {
          dialog.show()
          dialog.style.zIndex = 9999999
        } else {
          dialog.showModal()
        }
        // display the dialog at the right location
        // needed for the polyfill, otherwise it's not at the right position
        const windowHeight = window.innerHeight;
        if (this.dialogRef) {
          const dialogHeight = this.dialogRef.clientHeight;
          this.dialogRef.style.position = 'fixed';
          this.dialogRef.style.top = `${(windowHeight - dialogHeight) / 2}px`;
        }
      } else {
        dialog.close();
      }
    }
  }
}

Dialog.propTypes['modal'] = React.PropTypes.bool

export default Dialog