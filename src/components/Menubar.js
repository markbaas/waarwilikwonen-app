import React, { PropTypes } from 'react'
import AppBar from 'material-ui/AppBar'


const Menubar = ({ onMenuTap }) => (
    <AppBar
        title="Waar wil ik wonen?"
        onLeftIconButtonTouchTap={onMenuTap}
        />
)

Menubar.propTypes = {
    onMenuTap: PropTypes.func.isRequired,
}


export default Menubar
