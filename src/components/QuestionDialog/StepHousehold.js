import React from 'react'
import { Textfield } from 'react-mdl'
import Select from '../widgets/Select'
import Option from '../widgets/Option'

import BaseStep from './BaseStep'
import _ from '../../translate'


class StepHousehold extends BaseStep {
  renderStep() {
    return (
      <div>
        <Textfield
          label={_`What's your age?`}
          floatingLabel
          pattern="[0-9]"
          type="number"
          onChange={(event) => this.setState({ age: event.target.value })}
          required={this.state._validating}
          value={this.state.age || ''}
        />
        <div style={{
          color: 'rgba(0, 0, 0, 0.298039)',
          marginTop: '14px',
        }}>
          Do you have children?
        </div>
        <Textfield
          label={_`< 4 yrs`}
          floatingLabel
          pattern="[0-9]"
          type="number"
          style={{width: '30%'}}
          onChange={(event) => this.setState({ children4: event.target.value })}
          value={this.state.children4 || ''}
        />
        <Textfield
          label={_`4 - 12 yrs`}
          floatingLabel
          pattern="[0-9]"
          type="number"
          style={{width: '30%'}}
          onChange={(event) => this.setState({ children12: event.target.value })}
          value={this.state.children12 || ''}
        />
        <Textfield
          label={_`12 - 18 yrs`}
          floatingLabel
          pattern="[0-9]"
          type="number"
          style={{width: '30%'}}
          onChange={(event) => this.setState({ children18: event.target.value })}
          value={this.state.children18 || ''}
        />
        <Select
          label={_`Do you have a partner?`}
          floatingLabel
          onChange={(hasPartner) => this.setState({ hasPartner })}
          value={this.state.hasPartner}
          style={{zIndex: 200000}}
        >
          <Option value={true}>{_`Yes`}</Option>
          <Option value={false}>{_`No`}</Option>
        </Select>
        {(this.state.hasPartner) &&
          <Textfield
            label={_`What is your partner's age?`}
            floatingLabel
            pattern="[0-9]"
            type="number"
            required={this.state._validating}
            onChange={(event) => this.setState({ partnerAge: event.target.value })}
            value={this.state.partnerAge || ''}
          />
        }
      </div>
    )
  }
}

export default StepHousehold
