import React, { Component, PropTypes } from 'react'

import { DialogContent, DialogTitle, DialogActions } from 'react-mdl'
import Dialog from '../widgets/Dialog'

import StepHousehold from './StepHousehold'
import StepHousing from './StepHousing'
import StepWork from './StepWork'
import StepLeisure from './StepLeisure'
import StepPreferences from './StepPreferences'
import StepFinished from './StepFinished'

import _ from '../../translate'

const compactVersion = window.screen.width < 800


const styles = {}
styles.contentStyle = (compactVersion) ?
  {
    width: '100%',
    maxWidth: 'none',
    position: 'absolute',
    top: 0,
    transform: 'translate(0, 0)',
    height: '100vh',
    overflowY: 'scroll',
    opacity: 0.7
  } : {

  }
styles.bodyStyle = (compactVersion) ?
  {
    maxHeight: 'none',
    overflowY: 'none',
    padding: '0 5px 5px 5px'
  } : {

  }



class QuestionDialog extends Component {
  constructor() {
    super()
    this.state = {
      stepIndex: 0,
      open: true,
      household: {
        hasPartner: true,
        age: '30',
        children4: 1,
        partnerAge: '35'
      },
      housing: {
        budget: 1000,
        modality: 'rent',
        type: 0
      },
      leisure: {
        entertainment: [{label: 'Bars and Pubs', value: 'bars'}],
        sports: [{label: 'soccer', value: 'soccer'}]
      },
      work: {
        _hasWork: true,
        _partnerHasWork: false,
        educationLevel: 'wo',
        keywords: 'python'
      },
      preferences: {
        selected: ['public_transport', 'crime', 'nature']
      }
    }
  }

  stepBack() {
    const { stepIndex } = this.state;
    if (stepIndex !== 0) {
      this.setState({ stepIndex: stepIndex - 1})
    }
  }

  getStepContent(stepIndex) {
    const { onFinished, onClose } = this.props
    switch (stepIndex) {
      case 0:
        return (
          <StepHousehold onNextStep={
            (household) => this.setState({ household, stepIndex: stepIndex + 1 })}
            values={this.state.household}
          />
        )
      case 1:
        return (
          <StepHousing onNextStep={
            (housing) => this.setState({ housing, stepIndex: stepIndex + 1 })}
            onPreviousStep={this.stepBack.bind(this)}
            values={this.state.housing}
          />
        )
      case 2:
        return (
          <StepWork
            onNextStep={
              (work) => this.setState({ work, stepIndex: stepIndex + 1 })}
            onPreviousStep={this.stepBack.bind(this)}
            hasPartner={this.state.household.hasPartner}
            values={this.state.work}
          />
        )
      case 3:
        return (
          <StepLeisure onNextStep={
            (leisure) => this.setState({ leisure, stepIndex: stepIndex + 1 })}
            onPreviousStep={this.stepBack.bind(this)}
            values={this.state.leisure}
          />
        )
      case 4:
        return (
          <StepPreferences onNextStep={
            (preferences) => this.setState({ preferences, stepIndex: stepIndex + 1 })}
            onPreviousStep={this.stepBack.bind(this)}
            values={this.state.preferences}
          />
        )
      case 5:

        return (
          <StepFinished
            onPreviousStep={this.stepBack.bind(this)}
            onFinished={(token) => {
              const clean = (obj) => {
                const newObj = {}
                Object.keys(obj).filter((k) => k.match(/^[^_]/)).forEach((k) => {
                  newObj[k] = obj[k]
                })
                return newObj
              }
              onFinished({
                household: clean(this.state.household),
                housing: clean(this.state.housing),
                work: clean(this.state.work),
                leisure: {
                  entertainment: this.state.leisure.entertainment.map((x) => x.value),
                  sports: this.state.leisure.sports.map((x) => x.value)
                },
                preferences: this.state.preferences.selected
              }, token)
            }}
            onClose={onClose}
          />
        )
      default:
        return 'You\'re a long way from home sonny jim!';
    }
  }

  getStepTitle(stepIndex) {
    switch(stepIndex) {
      case 0:
        return _`Tell us about your family...`
      case 1:
        return _`How would your like to live?`
      case 2:
        return _`Tell us about work or education...`
      case 3:
        return _`What do you do for fun?`
      case 4:
        return _`What's most important to you?`
      case 5:
        return _`Ready!`
      default:
        return 'Whoops'
    }
  }

  render() {
    const { stepIndex } = this.state;
    return (
      <Dialog open={this.props.open} ref="QuestionDialog" style={{width: '90%', maxWidth: '300px'}}>
        <DialogTitle>{this.getStepTitle(stepIndex)}</DialogTitle>
        <DialogContent>
          {this.getStepContent(stepIndex)}
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    );
  }
}

QuestionDialog.propTypes = {
  onFinished: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool
}

export default QuestionDialog