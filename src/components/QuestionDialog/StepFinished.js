import React from 'react'

import BaseStep from './BaseStep'

import _ from '../../translate'

class StepFinished extends BaseStep {
  renderStep() {
    return (
      <div>
        <div>{_`We have collected all necessary data. Click finish to load your map!`}</div>
      </div>
    )
  }
}

export default StepFinished