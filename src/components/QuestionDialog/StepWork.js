import React, { PropTypes } from 'react'

import { Textfield } from 'react-mdl'

import Select from '../widgets/Select'
import Option from '../widgets/Option'

import BaseStep from './BaseStep'

import _ from '../../translate'


class StepWork extends BaseStep {
  constructor() {
    super()
    this.state = {}
  }
  renderStep() {
    const { hasPartner } = this.props
    return (
      <div>
        <Select
          label={_`What is your level of education?`}
          floatingLabel
          onChange={(educationLevel) => this.setState({ educationLevel })}
          value={this.state.educationLevel}
          required={this.state.validating}
        >
          <Option value={`primary`}>{_`Primary School`}</Option>
          <Option value={`secondary`}>{_`High School`}</Option>
          <Option value={`mbo`}>{_`Secondary Vocational Education`}</Option>
          <Option value={`hbo`}>{_`Higher Professional Education`}</Option>
          <Option value={`wo`}>{_`University`}</Option>
        </Select>
        <Select
          label={_`Do you work?`}
          floatingLabel
          onChange={(_hasWork) => this.setState({ _hasWork })}
          value={this.state._hasWork}
          required={this.state.validating}
        >
          <Option value={true}>{_`Yes`}</Option>
          <Option value={false}>{_`No`}</Option>
        </Select>
        {(this.state._hasWork) &&
          <Textfield
            label={_`Keywords describing your work?`}
            floatingLabel
            onChange={(event) => this.setState({keywords: event.target.value})}
            value={this.state.keywords}
            required={this.state.validating}
          />
        }
        {(hasPartner) &&
          <div>
            <Select
              label={_`Does your partner work?`}
              floatingLabel
              onChange={(_partnerHasWork) => this.setState({ _partnerHasWork })}
              value={this.state._partnerHasWork}
              required={this.state.validating}
            >
              <Option value={true}>{_`Yes`}</Option>
              <Option value={false}>{_`No`}</Option>
            </Select>
            {(this.state._partnerHasWork) &&
              <Textfield
                label={_`Keywords describing your partner's work?`}
                floatingLabel
                onChange={(event) => this.setState({partnerKeywords: event.target.value})}
                value={this.partnerKeywords}
                required={this.state.validating}
              />
            }
          </div>
        }
      </div>
    )
  }
}

StepWork.propTypes = {
    hasPartner: PropTypes.bool
}

export default StepWork