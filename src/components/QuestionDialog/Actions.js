import React, { PropTypes } from 'react'
import { Button } from 'react-mdl'
import RecaptchaButton from '../widgets/RecaptchaButton'

import _ from '../../translate'


const Actions = ({ onNextStep, onPreviousStep, onFinished, onClose }) => {
  return (
  <div style={{marginTop: 12}} className="action-container">
    {(onPreviousStep) &&
      <Button onClick={onPreviousStep}>{_`Back`}</Button>
    }
    {(onNextStep) &&
      <Button raised primary onClick={onNextStep}>{_`Next`}</Button>
    }
    {(onFinished) &&
      <RecaptchaButton
        raised primary
        sitekey={config.recaptcha_sitekey}
        callback={onFinished}
        onClick={onClose}
        >{_`Finished`}</RecaptchaButton>
    }
  </div>
)}

Actions.propTypes = {
    onNextStep: PropTypes.func,
    onPreviousStep: PropTypes.func,
    onFinished: PropTypes.func,
    onClose: PropTypes.func
}


export default Actions