import { connect } from 'react-redux'

import Menubar from '../components/Menubar'

import { openMenu } from '../actions'


const mapDispatchToProps = (dispatch) => {
    return {
        onMenuTap: () => {
            dispatch(openMenu(true))
        }
    }
}

const MenubarContainer = connect(
    null,
    mapDispatchToProps
)(Menubar)

export default MenubarContainer
