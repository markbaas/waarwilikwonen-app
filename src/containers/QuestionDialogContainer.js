import { connect } from 'react-redux'

import { setParams } from '../actions/data'
import { dialogChanged, validateRecaptcha } from '../actions/misc'

import QuestionDialog from '../components/QuestionDialog/index'


const mapStateToProps = (state) => {
  return {
    open: state.dialog
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFinished: (data, token) => {
      dispatch(validateRecaptcha(token))
      dispatch(setParams(data))
    },
    onClose: () => {
      dispatch(dialogChanged(false))
    }
  }
}

const QuestionDialogContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionDialog)

export default QuestionDialogContainer
