
export const receiveVectorLayer = (layer, url) => {
  return {
    type: 'RECEIVE_VECTOR_LAYER',
    layer,
    url,
    receivedAt: Date.now()
  }
}


export const fetchVectorLayer = () => {
  return (dispatch, getState) => {
    const token = getState().apikey.apikey
    dispatch(receiveVectorLayer('regions', `${config.api_url}/{z}/{x}/{y}/regions.pbf?token=${token}`))
  }
}